export interface Profile {
  id: string;
  name: string;
  lastName: string;
  role: string;
  branch: string;
  sector: string;
  days: number;
  username: string;
  password: string;
}

export interface MyContextType {
  selectedEmployee: string;
  setSelectedEmployee: (data: string) => void;
  loggedEmployee: string;
  setLoggedEmployee: (data: string) => void;
  profiles: Profile[];
}

export interface VacationData {
  // from: string | Date;
  // to: string | Date;
  // middayCheckout?: boolean;
  vacayType: string;
  startDate: string; // Adjust the type according to your actual data type
  endDate: string; // Adjust the type according to your actual data type
}
