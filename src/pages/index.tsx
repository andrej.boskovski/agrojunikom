import React, { useContext, useState } from "react";
import Input from "@/components/Input/Input";
import Head from "next/head";
import Image from "next/image";
import { MyContext } from "@/context/context";
import { useRouter } from "next/router";
import { Profile } from "@/interfaces/interface";

export default function Home() {
  const {
    selectedEmployee,
    setSelectedEmployee,
    loggedEmployee,
    setLoggedEmployee,
    profiles,
  }: any = useContext(MyContext);
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [error, setError] = useState<boolean>(false);
  const router = useRouter();

  const handleUsernameChange = (value: string) => {
    setUsername(value);
  };

  const handlePasswordChange = (value: string) => {
    setPassword(value);
  };

  const handleFocus = () => {
    setError(false);
  };

  return (
    <>
      <Head>
        <title>Agro Junikom</title>
        <meta name="description" content="Agro Junikom HR" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div
        className=" w-100 h-screen flex justify-center lg:justify-end items-center"
        style={{
          backgroundImage: `url("/images/agro-outline.png")`,
          backgroundSize: "contain",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "left bottom",
        }}
      >
        <div className="w-full lg:w-6/12 flex items-center justify-center">
          <div className="login bg-white w-11/12 md:w-8/12 h-96 rounded-xl shadow-lg flex flex-col items-center justify-center">
            <Image
              src={"/images/logo.png"}
              alt={"Logo"}
              height={80}
              width={250}
            />
            <form
              className="flex flex-col items-center justify-center gap-4 w-11/12 my-3"
              onSubmit={(e) => {
                e.preventDefault();
                const foundProfile = profiles.find(
                  (profile: any) =>
                    profile.username === username &&
                    profile.password === password
                );

                if (foundProfile) {
                  // If a profile is found, set the necessary state and navigate to the dashboard
                  router.push("dashboard");
                  setSelectedEmployee(foundProfile.id);
                  setLoggedEmployee(foundProfile.id);
                  setError(false);
                } else {
                  // If no profile is found, show an error message
                  setError(true);
                }
              }}
            >
              <Input
                type="text"
                placeholder="Корисничко име"
                value={username}
                onChange={handleUsernameChange}
                handleFocus={handleFocus}
              />
              <Input
                type="password"
                placeholder="Лозинка"
                icon={true}
                value={password}
                onChange={handlePasswordChange}
                handleFocus={handleFocus}
              />
              {error ? (
                <small className="flex color-red-agr">
                  Погрешно корисничко име или лозника
                </small>
              ) : null}

              <button
                type="submit"
                className="bg-red-agr text-white px-3 py-2 rounded-lg hover:bg-red-600"
              >
                Логирај се
              </button>
            </form>
            <a
              className="inline-block align-baseline font-bold text-sm text-red-600 hover:text-red-500"
              href="#"
            >
              Заборавена лозинка?
            </a>
          </div>
        </div>
      </div>
    </>
  );
}
