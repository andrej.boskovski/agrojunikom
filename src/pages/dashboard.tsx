import Header from "@/components/Header/Header";
import Select from "@/components/Select/Select";
import { MyContext } from "@/context/context";
import Calendar from "@/components/Calendar/Calendar";
import Head from "next/head";
import { useContext, useState } from "react";
import Footer from "@/components/Footer/Footer";
import ApplicationForm from "@/components/ApplicationForm/ApplicationForm";
import Hr from "@/components/Hr/Hr";
import Table from "@/components/Table/Table";
import { Profile, VacationData } from "@/interfaces/interface";

const Dashboard = () => {
  const [data, setData] = useState<VacationData[]>([]);

  const pushData = (
    vacayType: string,
    startDate: string,
    endDate: string,
    selectedEmployee: Profile[]
  ) => {
    const newDataObject = {
      selectedEmployee: selectedEmployee,
      vacayType: vacayType,
      startDate: startDate,
      endDate: endDate,
    };

    setData([...data, newDataObject]);
  };

  const {
    selectedEmployee,
    setSelectedEmployee,
    loggedEmployee,
    setLoggedEmployee,
    profiles,
  }: any = useContext(MyContext);

  const currentLoogedEmployee = profiles.filter(
    (profile: { id: string }) => profile.id === loggedEmployee
  );

  const [selectedOptionValue, setSelectedOptionValue] = useState<string>("");
  const [startDateValue, setStartDateValue] = useState();
  const [endDateValue, setEndDateValue] = useState();
  const [commentValue, setCommentDataValue] = useState();
  const [vacayDays, setVacayDays] = useState(1);

  return (
    <>
      <Head>
        <title>Agro Junikom</title>
        <meta name="description" content="Agro Junikom HR" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="bg-gray-50 h-screen">
        <Header selectedEmployee={selectedEmployee} profiles={profiles} />

        <Hr />

        {currentLoogedEmployee[0].role === "superadmin" ||
        currentLoogedEmployee[0].role === "admin" ? (
          <div>
            <Select
              profiles={profiles}
              selectedEmployee={selectedEmployee}
              setSelectedEmployee={setSelectedEmployee}
            />
            <Hr />
          </div>
        ) : null}

        <ApplicationForm
          setSelectedOptionValue={setSelectedOptionValue}
          setStartDateValue={setStartDateValue}
          setEndDateValue={setEndDateValue}
          pushData={pushData}
          profiles={profiles}
          selectedEmployee={selectedEmployee}
          vacayDays={vacayDays}
          setCommentValue={setCommentDataValue}
        />

        <Hr />

        <Calendar data={data} />
        <Hr />

        <Table
          vacayType={selectedOptionValue}
          startDate={startDateValue}
          endDate={endDateValue}
          data={data}
          selectedEmployee={selectedEmployee}
          setVacayDays={setVacayDays}
          comment={commentValue}
        />

        <Hr />

        <div>
          <Footer />
        </div>
      </div>
    </>
  );
};

export default Dashboard;
