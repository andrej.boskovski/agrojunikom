import { Profile } from "@/interfaces/interface";
import React, { useState } from "react";
import { Tooltip } from "flowbite-react";

interface Props {
  vacayType: string;
  startDate: any;
  endDate: any;
  data: any;
  comment: any;
  selectedEmployee: Profile[];
  setVacayDays: (value: number) => void;
}

const Table = ({
  vacayType,
  startDate,
  endDate,
  data,
  comment,
  selectedEmployee,
  setVacayDays,
}: Props) => {
  const vacationData = [
    {
      selectedEmployee: selectedEmployee,
      vacayType: vacayType,
      startDate: startDate,
      endDate: endDate,
    },
  ];

  function getWorkingDays(startDate: any, endDate: any) {
    // Copy the start and end dates
    const start = new Date(startDate);
    const end = new Date(endDate);

    // Initialize counters
    let workingDays = 0;
    let currentDate = start;

    // Loop through each day and count working days
    while (currentDate <= end) {
      const dayOfWeek = currentDate.getDay();
      if (dayOfWeek !== 0 && dayOfWeek !== 6) {
        workingDays++;
      }
      currentDate.setDate(currentDate.getDate() + 1);
    }

    return workingDays;
  }

  setVacayDays(getWorkingDays(startDate, endDate));

  return (
    <div className="bg-red-agr rounded-lg mt-3 mb-24 w-11/12 m-auto">
      {/* Table */}
      {startDate ? (
        <div>
          <div className="flex justify-between items-center p-2">
            <p className="text-white">Пријавени отсуства</p>
            {/* Search filter */}
            <input
              className=" py-1 px-3 w-3/12 rounded-lg shadow-lg"
              type="text"
              placeholder="Пребарувај..."
            />
          </div>

          <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
            <table className="w-full text-sm text-left text-gray-500">
              <thead className="text-xs text-gray-700 uppercase bg-gray-50">
                <tr>
                  <th scope="col" className="px-6 py-3">
                    Име и презиме
                  </th>
                  <th scope="col" className="px-6 py-3">
                    Вид отсуство
                  </th>
                  <th scope="col" className="px-6 py-3">
                    Почетна дата
                  </th>
                  <th scope="col" className="px-6 py-3">
                    Крајна дата
                  </th>
                  <th scope="col" className="px-6 py-3">
                    Денови
                  </th>
                  <th scope="col" className="px-6 py-3">
                    Забелешка
                  </th>
                  <th scope="col" className="px-6 py-3">
                    Статус
                  </th>
                  <th scope="col" className="px-6 py-3">
                    Потврда
                  </th>
                  <th scope="col" className="px-6 py-3">
                    Промена
                  </th>
                </tr>
              </thead>
              <tbody>
                {data.map((vacation: any, index: any) => (
                  <tr key={index} className="bg-white border-b">
                    <td className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                      {vacation.selectedEmployee[0].name +
                        " " +
                        vacation.selectedEmployee[0].lastName}
                    </td>
                    <td
                      scope="row"
                      className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap"
                    >
                      {vacation.vacayType}
                    </td>
                    <td className="px-6 py-4">
                      {new Date(vacation.startDate).toLocaleDateString("en-GB")}
                    </td>
                    <td className="px-6 py-4">
                      {new Date(vacation.endDate).toLocaleDateString("en-GB")}
                    </td>
                    <td className="px-6 py-4">
                      {getWorkingDays(vacation.startDate, vacation.endDate)}
                    </td>
                    <td className="px-6 py-4">
                      <Tooltip content={comment} className="bg-red-agr">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          height="16"
                          width="16"
                          viewBox="0 0 512 512"
                        >
                          <path d="M512 240c0 114.9-114.6 208-256 208c-37.1 0-72.3-6.4-104.1-17.9c-11.9 8.7-31.3 20.6-54.3 30.6C73.6 471.1 44.7 480 16 480c-6.5 0-12.3-3.9-14.8-9.9c-2.5-6-1.1-12.8 3.4-17.4l0 0 0 0 0 0 0 0 .3-.3c.3-.3 .7-.7 1.3-1.4c1.1-1.2 2.8-3.1 4.9-5.7c4.1-5 9.6-12.4 15.2-21.6c10-16.6 19.5-38.4 21.4-62.9C17.7 326.8 0 285.1 0 240C0 125.1 114.6 32 256 32s256 93.1 256 208z" />
                        </svg>
                      </Tooltip>
                    </td>

                    <td className="px-6 py-4 text-yellow-500">
                      Чека одобрување
                    </td>
                    <td className="px-6 py-4">
                      {/* Download file */}
                      <div className="flex ml-3 gap-3">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          height="16"
                          width="12"
                          viewBox="0 0 384 512"
                          className="cursor-pointer"
                        >
                          <path d="M64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V160H256c-17.7 0-32-14.3-32-32V0H64zM256 0V128H384L256 0zM216 232V334.1l31-31c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-72 72c-9.4 9.4-24.6 9.4-33.9 0l-72-72c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l31 31V232c0-13.3 10.7-24 24-24s24 10.7 24 24z" />
                        </svg>
                        {/* Print */}
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          height="16"
                          width="16"
                          viewBox="0 0 512 512"
                          className="cursor-pointer"
                        >
                          <path d="M128 0C92.7 0 64 28.7 64 64v96h64V64H354.7L384 93.3V160h64V93.3c0-17-6.7-33.3-18.7-45.3L400 18.7C388 6.7 371.7 0 354.7 0H128zM384 352v32 64H128V384 368 352H384zm64 32h32c17.7 0 32-14.3 32-32V256c0-35.3-28.7-64-64-64H64c-35.3 0-64 28.7-64 64v96c0 17.7 14.3 32 32 32H64v64c0 35.3 28.7 64 64 64H384c35.3 0 64-28.7 64-64V384zM432 248a24 24 0 1 1 0 48 24 24 0 1 1 0-48z" />
                        </svg>
                      </div>
                    </td>
                    <td className="px-6 py-4 ">
                      <div className="flex gap-3 ml-2">
                        {/* Izmeni */}
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          height="16"
                          width="16"
                          viewBox="0 0 512 512"
                          className="fill-yellow-500 cursor-pointer"
                        >
                          <path d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.7 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z" />
                        </svg>

                        {/* Izbrisi */}
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          height="16"
                          width="14"
                          viewBox="0 0 448 512"
                          className="fill-red-600 cursor-pointer"
                        >
                          <path d="M135.2 17.7L128 32H32C14.3 32 0 46.3 0 64S14.3 96 32 96H416c17.7 0 32-14.3 32-32s-14.3-32-32-32H320l-7.2-14.3C307.4 6.8 296.3 0 284.2 0H163.8c-12.1 0-23.2 6.8-28.6 17.7zM416 128H32L53.2 467c1.6 25.3 22.6 45 47.9 45H346.9c25.3 0 46.3-19.7 47.9-45L416 128z" />
                        </svg>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      ) : (
        <div className="flex justify-between items-center p-3">
          <p className="text-white">Нема пријавени отсуства</p>
        </div>
      )}
    </div>
  );
};

export default Table;
