import React from "react";

const Hr = () => {
  return <hr className="w-11/12 m-auto my-2" />;
};

export default Hr;
