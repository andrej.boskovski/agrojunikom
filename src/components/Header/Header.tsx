import React from "react";
import Image from "next/image";
import Link from "next/link";
import { Profile } from "@/interfaces/interface";

interface Props {
  selectedEmployee: string;
  profiles: Profile[];
}

const Header = ({ selectedEmployee, profiles }: Props) => {
  const chosenProfile = profiles.filter(
    (profile) => profile.id === selectedEmployee
  );

  return (
    <>
      <header className="flex justify-between w-11/12 m-auto mt-5 ">
        <div className="user w-12/12 flex items-center justify-center">
          <div className="w-16 h-16 border-2 border-red-agr border-neutral-800 rounded-full flex justify-center items-center relative">
            <p className="font-extrabold color-blue-agr">
              {chosenProfile[0].name[0]}
              {chosenProfile[0].lastName[0]}
            </p>
            {chosenProfile[0].role === "superadmin" ||
            chosenProfile[0].role === "admin" ? (
              <div className="absolute rounded-full bg-blue-agr w-5 h-5 text-white top-0 right-0 flex justify-center items-center">
                <p className="text-sm">3</p>
              </div>
            ) : null}
          </div>
          <div className="pl-3 hidden md:block flex flex-col justify-between md:justify-start">
            <h1 className="text-xl md:text-2xl font-bold text-center md:text-left">
              {chosenProfile[0].name} {chosenProfile[0].lastName}
            </h1>
            <div className="flex items-center gap-1">
              <p className="text-sm md:text-lg text-center md:text-left flex flex-col md:flex-row">
                Преостанати слободни денови:
              </p>
              <p className="font-bold">{chosenProfile[0].days}</p>
            </div>
          </div>
        </div>
        <div className="flex md:hidden flex-col justify-between md:justify-start">
          <h1 className="text-xl md:text-2xl font-bold text-center md:text-left">
            {chosenProfile[0].name} {chosenProfile[0].lastName}
          </h1>
          <p className="text-sm md:text-lg text-center md:text-left flex flex-col md:flex-row">
            Преостанати слободни денови:
          </p>
          <span className="font-bold text-center">{chosenProfile[0].days}</span>
        </div>

        <div className="flex items-center gap-5 ">
          <Image
            src={"/images/logo.png"}
            alt={"Logo"}
            height={80}
            width={300}
            className="hidden lg:block"
          />
          <Link href="/">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              height="2em"
              viewBox="0 0 512 512"
              fill="#c7202d"
              className="scale-75"
            >
              <path d="M502.6 278.6c12.5-12.5 12.5-32.8 0-45.3l-128-128c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L402.7 224 192 224c-17.7 0-32 14.3-32 32s14.3 32 32 32l210.7 0-73.4 73.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0l128-128zM160 96c17.7 0 32-14.3 32-32s-14.3-32-32-32L96 32C43 32 0 75 0 128L0 384c0 53 43 96 96 96l64 0c17.7 0 32-14.3 32-32s-14.3-32-32-32l-64 0c-17.7 0-32-14.3-32-32l0-256c0-17.7 14.3-32 32-32l64 0z" />
            </svg>
          </Link>
        </div>
      </header>
    </>
  );
};

export default Header;
