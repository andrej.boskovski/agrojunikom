import React from "react";
import Image from "next/image";
import Hr from "../Hr/Hr";

const Footer = () => {
  return (
    <footer className="fixed bg-gray-50 w-full bottom-0 px-5">
      <Hr />
      <div className="w-11/12 m-auto flex justify-between items-center py-3">
        <p>Agro Junikom Dooel Skopje</p>
        <Image
          src={"/images/logo.png"}
          alt={"Logo"}
          height={30}
          width={150}
          className="hidden lg:block"
        />
      </div>
    </footer>
  );
};

export default Footer;
