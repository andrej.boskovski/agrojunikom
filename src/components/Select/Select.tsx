import { MyContext } from "@/context/context";
import { Profile } from "@/interfaces/interface";
import React, { useContext, useState } from "react";

interface Props {
  profiles: Profile[];
  selectedEmployee: string;
  setSelectedEmployee: (employee: string) => void;
}

const Select = ({ profiles, setSelectedEmployee }: Props) => {
  const [isDropdownOpen, setIsDropdownOpen] = useState<boolean>(false);
  const [employeeSearch, setEmployeeSearch] = useState<string>("");
  // const { selectedEmployee, setSelectedEmployee }: any = useContext(MyContext);

  return (
    <div className="flex flex-wrap md:flex-nowrap items-center w-11/12 m-auto">
      {/* Dropdown button */}
      <div className="relative w-full">
        <div
          onClick={(e) => {
            setIsDropdownOpen(!isDropdownOpen);
            setEmployeeSearch("");
          }}
          className="p-4 border my-3 cursor-pointer  w-full bg-white border-gray-300 rounded-xl flex justify-between items-center"
        >
          <p>Избери вработен</p>
          <svg
            className={`${
              isDropdownOpen ? "transform rotate-180" : "transform rotate-0"
            } transition-all duration-500`}
            xmlns="http://www.w3.org/2000/svg"
            height="1em"
            viewBox="0 0 320 512"
          >
            <path d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z" />
          </svg>
        </div>
        {/* Dropdown */}
        <div
          className={`${
            isDropdownOpen ? "block" : "hidden"
          } absolute mx-auto top-14 w-full duration-500 p-4 border my-3 bg-white border-gray-300 rounded-xl shadow-xl z-10`}
        >
          <ul>
            {/* Search */}
            <li className="w-full flex justify-between items-center border-b-2 w-full focus:border-b-2 pb-1 mb-2">
              <input
                type="text"
                placeholder="Пребарај вработен"
                className="w-11/12 outline-none border-0 focus:border-0 focus:outline-none"
                value={employeeSearch}
                onChange={(e) => setEmployeeSearch(e.target.value)}
              />

              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="1em"
                viewBox="0 0 512 512"
                className="mr-2"
              >
                <path d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM208 352a144 144 0 1 0 0-288 144 144 0 1 0 0 288z" />
              </svg>
            </li>
            {/* Employees */}
            {profiles
              .filter(
                (employee) =>
                  employee.name
                    .toLowerCase()
                    .includes(employeeSearch.toLowerCase()) ||
                  employee.lastName
                    .toLowerCase()
                    .includes(employeeSearch.toLowerCase())
              )
              .map((profile) => (
                <li
                  onClick={() => {
                    setIsDropdownOpen(false);
                    setSelectedEmployee(profile.id);
                    setEmployeeSearch("");
                  }}
                  className="border-b-2 last:border-b-0 pb-1 last:pb-0 mb-1 last:mb-0 flex justify-between cursor-pointer hover:font-medium"
                  key={profile.id}
                >
                  <span>
                    {profile.name} {profile.lastName}
                  </span>
                  <span className="text-gray-400">{profile.branch}</span>
                </li>
              ))}
          </ul>
        </div>
      </div>
      {/* Buttons */}
      <div className="buttons flex justify-end gap-3 w-full md:w-5/12 md:ml-3">
        <button className="border border-red-agr p-3 rounded-xl hover:shadow-lg truncate overflow-ellipsis whitespace-no-wrap w-6/12">
          Додади вработен
        </button>
        <button className="bg-red-agr border border-red-agr text-white p-3 rounded-xl hover:shadow-lg hover: border-2 truncate overflow-ellipsis whitespace-no-wrap w-6/12">
          Избриши вработен
        </button>
      </div>
    </div>
  );
};

export default Select;
