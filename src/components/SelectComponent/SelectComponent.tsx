import React, { useState } from "react";

interface Props {
  value: string;
  onChange: (value: string) => void;
}

const SelectComponent = ({ value, onChange }: Props) => {
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);

  const vacationsType = [
    { id: "1", type: "Годишен одмор" },
    { id: "2", type: "Ден од одмор" },
    { id: "3", type: "Боледување" },
    { id: "4", type: "Друго" },
  ];

  const handleSelect = (selectedValue: string) => {
    onChange(selectedValue);
    setIsDropdownOpen(false);
  };

  return (
    <div className="flex justify-between items-center w-full m-auto gap-3">
      {/* Dropdown button */}
      <div className="relative w-full">
        <div
          onClick={() => {
            setIsDropdownOpen(!isDropdownOpen);
          }}
          className="p-4 border my-3 cursor-pointer w-full bg-white border-gray-300 rounded-xl flex justify-between items-center"
        >
          <p>{value ? value : "Избери вид на отсуство од работа"}</p>
          <svg
            className={`${
              isDropdownOpen ? "transform rotate-180" : "transform rotate-0"
            } transition-all duration-500`}
            xmlns="http://www.w3.org/2000/svg"
            height="1em"
            viewBox="0 0 320 512"
          >
            <path d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z" />
          </svg>
        </div>
        {/* Dropdown */}
        <div
          className={`${
            isDropdownOpen ? "block" : "hidden"
          } absolute mx-auto top-14  p-4 border my-3 w-full bg-white border-gray-300 rounded-xl shadow-xl`}
        >
          <ul>
            {/* Employees */}
            {vacationsType.map((vac) => (
              <li
                onClick={() => handleSelect(vac.type)}
                className="border-b-2 last:border-b-0 pb-1 last:pb-0 mb-1 last:mb-0 flex justify-between cursor-pointer hover:font-medium"
                key={vac.id}
              >
                <span>{vac.type}</span>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default SelectComponent;
