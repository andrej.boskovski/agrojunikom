import React, { useState } from "react";
import SelectComponent from "../SelectComponent/SelectComponent";
import { Profile } from "@/interfaces/interface";
import { Datepicker } from "flowbite-react";

interface Props {
  setSelectedOptionValue: (option: string) => void;
  setStartDateValue: (option: any) => void;
  setEndDateValue: (option: any) => void;
  setCommentValue: (option: any) => void;
  pushData: (
    option: string,
    from: any,
    to: any,
    selectedEmployee: Profile[],
    comment: string
  ) => void;
  profiles: Profile[];
  selectedEmployee: string;
  vacayDays: number;
}

const ApplicationForm = ({
  setSelectedOptionValue,
  setStartDateValue,
  setEndDateValue,
  setCommentValue,
  pushData,
  profiles,
  selectedEmployee,
  vacayDays,
}: Props) => {
  const [selectedOption, setSelectedOption] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [comment, setComment] = useState("");
  const [error, setError] = useState<boolean>(false);
  const [daysError, setDaysError] = useState<boolean>(false);

  const today = new Date().toLocaleDateString();

  const chosenProfile = profiles.filter(
    (profile) => profile.id === selectedEmployee
  );

  const handleSelectChange = (value: any) => {
    setSelectedOption(value);
  };

  const handleStartDateChange = (event: any) => {
    setStartDate(event.target.value);
    startDate ? setEndDate(startDate) : setEndDate(event.target.value);
  };

  const handleEndDateChange = (event: any) => {
    setEndDate(event.target.value);
  };

  const handleCommentChange = (event: any) => {
    setComment(event.target.value);
  };

  // Submit
  const handleSubmit = (event: any) => {
    event.preventDefault();

    if (!selectedOption || !startDate || !endDate) {
      setError(true);
      return;
    }

    // if (vacayDays === 0) {
    //   setDaysError(true);
    //   return;
    // }

    // setDaysError(false);

    setError(false);

    // Passing data to dashboard
    setSelectedOptionValue(selectedOption);
    setStartDateValue(startDate);
    setEndDateValue(endDate);
    setCommentValue(comment);

    // Push data to the data array
    pushData(selectedOption, startDate, endDate, chosenProfile, comment);

    // Clearing inputs
    setSelectedOption("");
    setStartDate("");
    setEndDate("");
    setComment("");
  };

  return (
    <div className="w-11/12 mt-3 m-auto">
      <div>
        <h1 className="font-bold">Барање за отсуство од работа</h1>
      </div>
      <form onSubmit={handleSubmit}>
        <div className="flex flex-col ">
          <SelectComponent
            value={selectedOption}
            onChange={handleSelectChange}
          />
          {error ? (
            <small className="color-red-agr -mt-3">Задолжително поле</small>
          ) : null}
        </div>
        <div className="flex flex-col w-full ">
          <label htmlFor="date" className="-mb-3">
            Дополнително објаснување на причината за отсуство.
          </label>
          <textarea
            className="p-4  border my-3 cursor-pointer w-full bg-white border-gray-300 rounded-xl flex justify-between items-center"
            name="textarea"
            id="textarea"
            placeholder="Причина"
            value={comment}
            onChange={handleCommentChange}
          ></textarea>
        </div>

        <div
          className={`flex flex-wrap ${
            error ? "" : "items-center"
          } md:flex-nowrap gap-1 md:gap-3`}
        >
          <div className="flex flex-col w-full md:w-6/12 lg:w-4/12">
            <label htmlFor="date" className="-mb-3">
              Почетна дата
            </label>
            <input
              type="date"
              value={startDate}
              onFocus={() => setError(false)}
              onChange={handleStartDateChange}
              className="p-4 border my-3 cursor-pointer w-full bg-white border-gray-300 rounded-xl flex justify-between items-center"
            />
            {error ? (
              <small className="color-red-agr -mt-3">Задолжително поле</small>
            ) : null}
          </div>
          <div className="flex flex-col w-full md:w-6/12 lg:w-4/12">
            <label htmlFor="date" className="-mb-3">
              Крајна дата
            </label>
            <input
              id="date"
              type="date"
              placeholder="YYYY-MM-DD"
              value={endDate}
              onFocus={() => setError(false)}
              onChange={handleEndDateChange}
              className="p-4 border my-3 cursor-pointer w-full bg-white border-gray-300 rounded-xl flex justify-between items-center"
            />
            {error ? (
              <small className="color-red-agr -mt-3">Задолжително поле</small>
            ) : null}
            {daysError ? (
              <small className="color-red-agr -mt-3">
                Крајната дата неможе да биде еднаква или помала од почетната
                дата
              </small>
            ) : null}
          </div>
          <div
            className={`w-full md:w-12/12 lg:w-4/12 ${error ? "mt-6" : "mt-2"}`}
          >
            <button
              type="submit"
              className="bg-red-agr w-full border border-red-agr text-white p-4 rounded-xl hover:shadow-lg hover:border-2"
            >
              Поднеси
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default ApplicationForm;
