import React from "react";

interface Props {
  title: string;
}

const Button = ({ title }: Props) => {
  return (
    <div
      className="bg-white shadow-lg rounded-lg p-10 text-xl hover:scale-105 transition-all hover:font-semibold"
      style={{ cursor: "pointer" }}
    >
      {title}
    </div>
  );
};

export default Button;
