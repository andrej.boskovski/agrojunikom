import { MyContextType } from "@/interfaces/interface";
import { ReactNode, createContext, useState } from "react";

interface MyContextProviderProps {
  children: ReactNode;
}

export const MyContext = createContext<MyContextType | undefined>(undefined);

export const MyContextProvider: React.FC<MyContextProviderProps> = ({
  children,
}) => {
  const [selectedEmployee, setSelectedEmployee] = useState("1");
  const [loggedEmployee, setLoggedEmployee] = useState("1");

  const profiles = [
    {
      id: "1",
      role: "superadmin",
      name: "Кристијан",
      lastName: "Стојоски",
      branch: "дирекција",
      username: "kiko",
      password: "123",
      sector: "topmanagement",
      days: 17,
    },
    {
      id: "2",
      role: "admin",
      name: "Сања",
      lastName: "Стојоска",
      branch: "дирекција",
      username: "sanja",
      password: "123",
      sector: "topmanagement",
      days: 21,
    },
    {
      id: "3",
      role: "user",
      name: "Емилија",
      lastName: "Аврамовска",
      branch: "дирекција",
      username: "emi",
      password: "123",
      sector: "secretary",
      days: 4,
    },
    {
      id: "4",
      role: "user",
      name: "Дејан",
      lastName: "Ставровски",
      branch: "драчево",
      username: "dejan",
      password: "123",
      sector: "sales",
      days: 6,
    },
  ];

  return (
    <MyContext.Provider
      value={{
        selectedEmployee,
        setSelectedEmployee,
        loggedEmployee,
        setLoggedEmployee,
        profiles,
      }}
    >
      {children}
    </MyContext.Provider>
  );
};
